# texnom

This is a library for basic multilingual text normalization.

[![pipeline status](https://gitlab.com/Jaco-Assistant/texnom/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/texnom/-/commits/master)
[![coverage report](https://gitlab.com/Jaco-Assistant/texnom/badges/master/coverage.svg)](https://gitlab.com/Jaco-Assistant/texnom/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/texnom/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/texnom/-/commits/master)

<br>

## Usage

Install from git repository:

```bash
pip3 install --upgrade git+https://gitlab.com/Jaco-Assistant/texnom
```

Cleaning texts

```python
from texnom import normalizer

alphabet = list("abcdefghijklmnopqrstuvwxyz' ")
normalizr = normalizer.Normalizer(language_code="en", allowed_chars=alphabet)

result = normalizr.clean_sentence("Can you name me 3 numbers?")
# can you name me three numbers

result = normalizr.clean_sentence_list(["1", "2", "and 3"])
# ["one", "two", "and three"]
```

<br>

## Testing

Install from a directory or branch:

```bash
pip3 install --user -e .
pip3 install --upgrade git+https://gitlab.com/Jaco-Assistant/texnom@my_branch
```

Build container with testing tools:

```bash
docker build --progress=plain -f tests/Containerfile -t testing_texnom .

docker run --network host --rm \
  --volume `pwd`/:/texnom/ \
  -it testing_texnom
```

Run unit tests:

```bash
pip3 install --user -e /texnom/
pytest /texnom/ --cov=texnom -v -s
```

For syntax tests, check out the steps in the [gitlab-ci](.gitlab-ci.yml#L75) file.
