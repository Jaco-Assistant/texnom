import json
import os
import re
from concurrent.futures import ProcessPoolExecutor as Pool
from functools import partial
from typing import List

import num2words
import tqdm

from . import basic_normalizer

# ==================================================================================================


class Normalizer:
    def __init__(self, allowed_chars: List[str], language_code: str = "xx") -> None:
        filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
        language_code = language_code.lower()
        self.allowed_chars = allowed_chars
        self.basic_norm = basic_normalizer.Normalizer(self.allowed_chars)

        # Check language code
        language_keys = sorted(os.listdir(filepath + "data/"))
        if language_code in language_keys:
            self.language = language_code
        elif language_code == "xx":
            self.language = "xx"
        else:
            msg = 'Language "{}" is not supported, using "xx" instead!'
            print("WARNING:", msg.format(language_code))
            self.language = "xx"

        # Load langdicts
        if self.language != "xx":
            path = filepath + "data/{}/langdicts.json".format(self.language)
            self.langdicts = self.load_json_file(path)

        # Regex patterns, see www.regexr.com for good explanation
        self.multi_space_pattern = re.compile(r"\s+")
        self.html_pattern_1 = re.compile(r"&#[0-9]+;|&nbsp;")
        self.html_pattern_2 = re.compile(r"<[^>]*>")

        # Check num2words availability
        self.use_num2words = False
        if self.language != "xx":
            try:
                num2words.num2words(123, lang=self.language)
                self.decimal_pattern = re.compile(
                    self.langdicts["number_pattern"]["decimal"]
                )
                self.ordinal_pattern = re.compile(
                    self.langdicts["number_pattern"]["ordinal"]
                )
                self.use_num2words = True
            except NotImplementedError:
                msg = 'Converting numbers is not supported for language "{}"'
                print("WARNING:", msg.format(self.language))

    # ==============================================================================================

    @staticmethod
    def load_json_file(path: str) -> dict:
        with open(path, "r", encoding="utf-8") as file:
            content: dict = json.load(file)
        return content

    # ==============================================================================================

    def clean_sentence(self, sentence: str) -> str:
        """Normalize the given sentence. Return a tuple (cleaned sentence)"""

        # Convert to lower and replace additional spaces
        sentence = sentence.lower()
        sentence = self.basic_norm.remove_duplicate_whitespaces(sentence)

        if self.language != "xx":
            sentence = self.replace_specials(sentence)

        words = sentence.split()
        cleaned_words = []
        for word in words:
            cleaned_word = self.clean_word(word)
            cleaned_words.append(cleaned_word)
        sentence = " ".join(cleaned_words)
        sentence = sentence.lower()

        sentence = self.basic_norm.remove_duplicate_whitespaces(sentence)
        return sentence

    # ==============================================================================================

    def clean_sentence_list(self, sentences: List[str]) -> List[str]:
        cl_func = partial(self.clean_sentence)

        with Pool() as p:
            processed_sentences = list(
                tqdm.tqdm(p.map(cl_func, sentences), total=len(sentences))
            )

        return processed_sentences

    # ==============================================================================================

    def replace_specials(self, text: str) -> str:
        """Apply special replacement rules to the given word."""

        replacers = self.langdicts["special_replacers"]
        for to_replace, replacement in replacers.items():
            text = text.replace(to_replace, " {} ".format(replacement))

        replacers = self.langdicts["spaceless_replacers"]
        for to_replace, replacement in replacers.items():
            text = text.replace(to_replace, "{}".format(replacement))

        return text

    # ==============================================================================================

    def word_to_num(self, word: str) -> str:
        """Replace numbers with their written representation."""

        matches = self.ordinal_pattern.findall(word)
        if len(matches) == 1:
            num_word = num2words.num2words(
                int(matches[0][:-1]), lang=self.language, to="ordinal"
            )
            word = word.replace(matches[0], " {} ".format(num_word))

        matches = self.decimal_pattern.findall(word)
        for match in matches:
            num = match.replace(".", "")
            num = num.replace(",", ".")
            num_word = num2words.num2words(float(num), lang=self.language)
            word = word.replace(match, " {} ".format(num_word))

        # Make word lowercase again
        word = word.lower()
        return word

    # ==============================================================================================

    def clean_word(self, word: str) -> str:
        """Clean the given word"""

        word = word.lower()
        word = word.strip()

        # Drop html codes
        matches = self.html_pattern_1.findall(word)
        for match in matches:
            word = word.replace(match, " ")

        # Drop html tags
        matches = self.html_pattern_2.findall(word)
        for match in matches:
            word = word.replace(match, " ")

        # Convert numbers to words
        if self.use_num2words:
            word = self.word_to_num(word)

        # Replace special words again, sometimes they are behind a number like '12kg' -> 'twelve kg'
        # Adding a space because replacer only looks for ' kg ' so that 'kg' is not replaced in words
        if self.language != "xx":
            word = self.replace_specials(" {} ".format(word))

        # Remove symbols and convert special characters to their base form
        # Drop all chars that are not explicitly allowed
        word = self.basic_norm.clean_word(word)

        return word
