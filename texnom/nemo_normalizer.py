from typing import List

from nemo_text_processing.text_normalization.normalize import Normalizer as NemoNorm

from .normalizer import Normalizer

# ==================================================================================================


class NemoNormalizer(Normalizer):
    def __init__(self, allowed_chars: List[str], language_code: str = "xx") -> None:
        super().__init__(allowed_chars, language_code)

        self.nemo_normalizer = None
        try:
            self.nemo_normalizer = NemoNorm(input_case="cased", lang=language_code)
        except NotImplementedError:
            msg = "Nemo doesn't support the language '{}', using default normalizer instead"
            print(msg.format(language_code))

    # ==============================================================================================

    def clean_sentence(self, sentence: str) -> str:
        """Normalize the given sentence. Return a tuple (cleaned sentence)"""

        # Call nemo's normalization tool
        if self.nemo_normalizer is not None:
            sentence = self.nemo_normalizer.normalize(
                sentence, verbose=False, punct_post_process=True
            )

        # Convert to lower and replace additional spaces
        sentence = sentence.lower()
        sentence = self.basic_norm.remove_duplicate_whitespaces(sentence)

        if self.language != "xx":
            sentence = self.replace_specials(sentence)

        words = sentence.split()
        cleaned_words = []
        for word in words:
            cleaned_word = self.clean_word(word)
            cleaned_words.append(cleaned_word)
        sentence = " ".join(cleaned_words)
        sentence = sentence.lower()

        sentence = self.basic_norm.remove_duplicate_whitespaces(sentence)
        return sentence
