import os
from typing import List

from texnom import basic_normalizer, nemo_normalizer, normalizer

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"

# ==================================================================================================


def load_alphabet(language: str) -> List[str]:
    ab_path = filepath + "data/{}/alphabet.json".format(language)
    ab_dict = normalizer.Normalizer.load_json_file(ab_path)
    ab_list = list(ab_dict)
    return ab_list


def load_normalizer(language: str) -> normalizer.Normalizer:
    alphabet = load_alphabet(language)
    normalizr = normalizer.Normalizer(language_code=language, allowed_chars=alphabet)
    return normalizr


# ==================================================================================================


def test_normalizations() -> None:
    language = "xx"
    alphabet = load_alphabet("en")
    normalizr = normalizer.Normalizer(language_code=language, allowed_chars=alphabet)
    text = "Löwenzahn"
    label = "lowenzahn"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "de"
    normalizr = load_normalizer(language)
    text = "Löwenzahn"
    label = "löwenzahn"
    result = normalizr.clean_sentence(text)
    assert label == result

    text = "Hi #, wie geht's dir?"
    label = "hi wie gehts dir"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "en"
    normalizr = load_normalizer(language)
    text = "I'dlik3much$$"
    label = "i'dlik three much dollar dollar"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "es"
    normalizr = load_normalizer(language)
    text = "Me pica el bagre"
    label = "me pica el bagre"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "fr"
    normalizr = load_normalizer(language)
    text = "Liberté, Égalité, Fraternité"
    label = "liberté égalité fraternité"
    result = normalizr.clean_sentence(text)
    assert label == result


# ==================================================================================================


def test_clean_sentence_list() -> None:
    language = "de"
    normalizr = load_normalizer(language)

    sentences = [
        "Hi, wie geht's dir?",
        "Möchtest du 3kg Vanilleeis?",
        "Ich habe leider nur 2€",
        "Der Preiß mag dafür 12.300,50€",
        "Für Vanilleeis? Da kauf ich lieber 1,5m² Grundstück in München",
    ]
    correct_sentences = [
        "hi wie gehts dir",
        "möchtest du drei kilogramm vanilleeis",
        "ich habe leider nur zwei euro",
        "der preiß mag dafür zwölftausenddreihundert komma fünf euro",
        "für vanilleeis da kauf ich lieber eins komma fünf quadratmeter grundstück in münchen",
    ]
    cleaned_sentences = normalizr.clean_sentence_list(sentences)

    assert cleaned_sentences == correct_sentences


# ==================================================================================================


def test_alphabets() -> None:
    for language in sorted(os.listdir(filepath + "data/")):
        normalizr = load_normalizer(language)
        alphabet = load_alphabet(language)
        text = "".join(alphabet)
        label = text.strip()
        result = normalizr.clean_sentence(text)
        assert label == result


# ==================================================================================================


def test_slu() -> None:
    alphabet = load_alphabet("en")
    alphabet.extend(list("[]()|->?_0123456789"))
    normalizr = basic_normalizer.Normalizer(allowed_chars=alphabet)

    sentences = [
        "(the Day after Tomorrow)->in two days",
        "(this| ) (saturday|sunday)",
        "ist die antwort [---](skill_riddles-riddle_answers?try1)",
    ]
    correct_sentences = [
        "(the day after tomorrow)->in two days",
        "(this| ) (saturday|sunday)",
        "ist die antwort [---](skill_riddles-riddle_answers?try1)",
    ]
    cleaned_sentences = [normalizr.clean_sentence(s) for s in sentences]

    assert cleaned_sentences == correct_sentences


# ==================================================================================================


def test_nemo_normalizer() -> None:
    language = "xx"
    alphabet = load_alphabet("en")
    normalizr = nemo_normalizer.NemoNormalizer(
        language_code=language, allowed_chars=alphabet
    )
    text = "Löwenzahn"
    label = "lowenzahn"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "de"
    alphabet = load_alphabet(language)
    normalizr = nemo_normalizer.NemoNormalizer(
        language_code=language, allowed_chars=alphabet
    )
    text = "Hi #, morgen ist der 23.06.2023."
    label = "hi morgen ist der drei und zwanzigster juni zwei tausend drei und zwanzig"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "en"
    alphabet = load_alphabet(language)
    normalizr = nemo_normalizer.NemoNormalizer(
        language_code=language, allowed_chars=alphabet
    )
    text = "Mr. Smith paid $111 in U.S.A. on Dec. 17th."
    label = "mister smith paid one hundred and eleven dollars in usa on december seventeenth"
    result = normalizr.clean_sentence(text)
    assert label == result

    language = "fr"
    alphabet = load_alphabet(language)
    normalizr = nemo_normalizer.NemoNormalizer(
        language_code=language, allowed_chars=alphabet
    )
    text = "Liberté, Égalité, Fraternité"
    label = "liberté égalité fraternité"
    result = normalizr.clean_sentence(text)
    assert label == result


# ==================================================================================================


def test_clean_sentence_list_nemo() -> None:
    language = "en"
    alphabet = load_alphabet(language)
    normalizr = nemo_normalizer.NemoNormalizer(
        language_code=language, allowed_chars=alphabet
    )

    sentences = [
        "We paid $123 for this desk.",
        "Mr. Smith paid $111 in U.S.A. on Dec. 17th.",
    ]
    correct_sentences = [
        "we paid one hundred and twenty three dollars for this desk",
        "mister smith paid one hundred and eleven dollars in usa on december seventeenth",
    ]
    cleaned_sentences = normalizr.clean_sentence_list(sentences)

    assert cleaned_sentences == correct_sentences
